/**
 * Compute the mean of the elements in the given array.
 * @param  {number[]} array the array
 * @param  {number}   start starting index
 * @param  {number}   n how many
 * @return {number}   the mean
 */
export const mean = (
  array: number[],
  start: number = 0,
  n: number = array.length
) => {
  let sum = 0;
  for (let i = start; i < start + n; i += 1) {
    sum += array[i];
  }
  return sum / n;
};

/**
 * Compute the variance of the elements in the given array.
 * @param  {number[]} array the array
 * @param  {number}   start starting index
 * @param  {number}   n how many
 * @param  {number}   inAvg if we already have it
 * @return {number}   the variance
 */
export const variance = (
  array: number[],
  start: number = 0,
  n: number = array.length,
  inAvg: number | null
) => {
  const avg = inAvg || mean(array, start, n);
  let sum = 0;
  for (let i = start; i < start + n; i += 1) {
    const diff = array[i] - avg;
    sum += diff ** 2;
  }
  return sum;
};

/**
 * Compute the standard deviation of the elements in the given array.
 * @param  {number[]} array the array
 * @param  {number}   start starting index
 * @param  {number}   n how many
 * @param  {number}   inAvg if we already have it
 * @return {number}   the standard deviation
 */
export const stdDev = (
  array: number[],
  start: number = 0,
  n: number = array.length,
  inAvg: number | null
) => Math.sqrt(variance(array, start, n, inAvg));

/**
 * Finds the median of the sorted array of numbers.
 * @param  {[number]} sorted the sorted array
 * @return {number}   the median
 */
export const median = (sorted: number[]) => {
  if (sorted.length % 2 === 0) {
    const mid = sorted.length / 2;
    return (sorted[mid - 1] + sorted[mid]) / 2;
  }
  return sorted[Math.floor(sorted.length / 2)];
};

/**
 * Finds the quartiles of the sorted array of numbers.
 * @param  {[number]} sorted the sorted array
 * @param  {Object}   { q1, q2, q3 }
 */
export const quartiles = (sorted: number[]) => {
  const q2 = median(sorted);
  let q1;
  let q3;
  const half = Math.floor(sorted.length / 2);
  if (sorted.length % 2 === 0) {
    q1 = median(sorted.slice(0, half));
    q3 = median(sorted.slice(half));
  } else {
    q1 = median(sorted.slice(0, half + 1));
    q3 = median(sorted.slice(half));
  }
  return { q1, q2, q3 };
};

/**
 * Get the local maxima and minima in the given buffer
 * @param  {number[]} buffer the buffer
 * @param  {number}   from starting index in buffer
 * @param  {number}   to ending index in buffer
 * @return {Object} { maxima, minima }
 */
export const criticalPoints = (
  buffer: Float32Array | number[],
  from: number = 0,
  to: number = buffer.length
) => {
  let risingCount = 0;
  let fallingCount = 0;
  let duplicateCount = 0;
  const maxima: number[] = [];
  const minima: number[] = [];
  for (let i = from + 1; i < buffer.length - 1 && i < to; i += 1) {
    if (buffer[i] < buffer[i + 1]) {
      risingCount += 1;
      if (fallingCount > 0) {
        minima[minima.length] = i - Math.round(duplicateCount / 2);
        fallingCount = 0;
      }
      duplicateCount = 0;
    } else if (buffer[i] > buffer[i + 1]) {
      fallingCount += 1;
      if (risingCount > 0) {
        maxima[maxima.length] = i - Math.round(duplicateCount / 2);
        risingCount = 0;
      }
      duplicateCount = 0;
    } else {
      duplicateCount += 1;
    }
  }
  return {
    maxima,
    minima
  };
};

/**
 * Computes the rolling average of the given buffer.
 * @param  {number[]} buffer the buffer
 * @param  {number}   startingAverage carry across calls to this method
 * @param  {number}   windowSize how many elements to use for the rolling average
 * @return {number[]} the smoothed window
 */
export const rollingAverage = (
  buffer: number[],
  startingAverage: number,
  windowSize: number
) => {
  const rolling = [];
  let average = startingAverage;
  for (let i = 0; i < buffer.length; i += 1) {
    average -= average / windowSize;
    average += buffer[i] / windowSize;
    rolling[rolling.length] = average;
  }
  return rolling;
};

export const closestPair = (maxima: number[]) => {
  if (maxima.length < 2) {
    return undefined;
  }
  let lower = 0;
  let min = maxima[1] - maxima[0];
  for (let i = 2; i < maxima.length; i += 1) {
    const delta = maxima[i] - maxima[i - 1];
    if (delta < min) {
      min = delta;
      lower = i - 1;
    }
  }
  return lower;
};
