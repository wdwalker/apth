import { KeyName, KEY_NAMES } from './Piano';

const A4 = KEY_NAMES.indexOf('A4');
const CONCERT_PITCH = 440;
const log2 = (n: number) => Math.log(n) / Math.log(2);

/**
 * A mathematically correct equal temperament.
 */
class EqualTemperament {
  static minHz: number = EqualTemperament.hz(KEY_NAMES[0], -50);
  static maxHz: number = EqualTemperament.hz(
    KEY_NAMES[KEY_NAMES.length - 1],
    50
  );

  /**
   * For the given keyName, find the Hz when the key is tuned to equal
   * temperament offset by the given cents
   * @param  keyName from KEY_NAMES
   * @param  cents   +/- offset
   * @returns Hz for equal temperament
   */
  static hz(keyName: KeyName, cents: number = 0): number {
    const keyNum = KEY_NAMES.indexOf(keyName);
    const semitonesFromA4 = keyNum - A4;
    const centsFromA4 = cents + semitonesFromA4 * 100;
    return 2 ** (centsFromA4 / 1200) * CONCERT_PITCH;
  }

  /**
   * For the given frequency, find the keyName (from KEY_NAMES) and
   * cents offset from perfect equal temperament.
   * @param  {number} hz the given frequency
   * @return {{ keyName: KeyName; cents: number }}
   */
  static keyAndCents(hz: number): { keyName: KeyName; cents: number } {
    const centsFromA4 = 1200 * log2(hz / CONCERT_PITCH);
    const keysFromA4 = Math.round(centsFromA4 / 100);
    let key = A4 + keysFromA4;
    let cents = centsFromA4 - 100 * keysFromA4;
    // Hack to handle boundary at A0 - 50c
    if (cents > 49.95) {
      cents -= 100;
      key += 1;
    }
    return {
      cents,
      keyName: KEY_NAMES[key]
    };
  }

  /**
   * Returns an array of theoretical harmonics for the given key
   * @param  {KeyName} keyName from KEY_NAMES
   * @param  {number}  num     the number of partial to find
   * @return {number[]}
   */
  static harmonics(keyName: KeyName, num: number = 12): number[] {
    const partials: number[] = [];
    const hz = EqualTemperament.hz(keyName, 0);
    for (let i = 0; i < num; i += 1) {
      partials[i] = hz * (i + 1);
    }
    return partials;
  }

  /**
   * Returns an array of the names of the keys (from KEY_NAMES) for
   * the harmonics of the given key
   * @param  {KeyName}  keyName from KEY_NAMES
   * @param  {number}   num     the number of partial to find
   * @return {KeyName[]}        array of KEY_NAMES
   */
  static harmonicKeyNames(keyName: KeyName, num: number = 12): KeyName[] {
    const keyNames: KeyName[] = [];
    const partials = EqualTemperament.harmonics(keyName, num);
    for (
      let i = 0;
      i < partials.length && partials[i] <= EqualTemperament.maxHz;
      i += 1
    ) {
      const keyAndCents = EqualTemperament.keyAndCents(partials[i]);
      keyNames[i] = keyAndCents.keyName;
    }
    return keyNames;
  }
}

export default EqualTemperament;
