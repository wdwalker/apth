import * as d3 from 'd3';

/**
 * A bunch of utility functions wrapped around D3.js.
 */

/**
 * A pretend polyfill.
 */
const sighn = (x: number) => {
  if (x < 0) {
    return -1;
  }
  if (x > 0) {
    return 1;
  }
  return 0;
};

/**
 * Draws the given signal (starting at offset) on the given
 * svgElement.
 * @param svgElement where to draw it
 * @param signal     the signal
 * @param xDomain    (min,max) of the X values
 * @param yDomain    (min,max) of the Y values
 * @param offset     index of where to start in signal
 */
export const drawSignal = (
  svgElement: SVGSVGElement,
  signal: Float32Array | number[],
  xDomain: [number, number],
  yDomain: [number, number],
  color: string = '#00ffff',
  offset: number = 0
) => {
  const x = d3
    .scaleLinear()
    .domain(xDomain)
    .range([0, svgElement.width.baseVal.value]);
  const y = d3
    .scaleLinear()
    .domain(yDomain)
    .range([svgElement.height.baseVal.value, 0]);

  const lineGenerator = d3
    .line<number>()
    .x((d, i) => x(i - offset))
    .y((d: number) => y(d));

  d3
    .select(svgElement)
    .append('g')
    .append('path')
    .datum(signal)
    .attr('fill', 'none')
    .attr('stroke', color)
    .attr('d', lineGenerator);
};

export const drawSignalMaxima = (
  svgElement: SVGSVGElement,
  signal: number[],
  maxima: number[],
  xDomain: [number, number],
  yDomain: [number, number],
  color: string = 'blue',
  offset: number
) => {
  const points: [number, number][] = [];
  for (let i = 0; i < maxima.length; i += 1) {
    points[i] = [maxima[i], signal[maxima[i]]];
  }
  const x = d3
    .scaleLinear()
    .domain(xDomain)
    .range([0, svgElement.width.baseVal.value]);
  const y = d3
    .scaleLinear()
    .domain(yDomain)
    .range([svgElement.height.baseVal.value, 0]);

  d3
    .select(svgElement)
    .append('g')
    .selectAll('dots')
    .data(points)
    .enter()
    .append('circle')
    .attr('class', 'dot')
    .attr('r', 3)
    .attr('cx', (d: number[], i: number) => x(d[0] - offset))
    .attr('cy', (d: number[], i: number) => y(d[1]))
    .style('fill', color);
};

export const drawGrid = (
  svgElement: SVGSVGElement,
  numDivs: number,
  offsetLeft: number = 0,
  offsetRight: number = 0,
  color: string = '#222'
) => {
  const width = svgElement.width.baseVal.value;
  const height = svgElement.height.baseVal.value;
  const step = (width - (offsetLeft + offsetRight)) / numDivs;
  for (let x = offsetLeft, key = 0; x < width; x += step, key += 1) {
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    line.setAttribute('x1', `${x}`);
    line.setAttribute('y1', '0');
    line.setAttribute('x2', `${x}`);
    line.setAttribute('y2', `${height - 1}`);
    line.setAttribute('stroke', color);
    line.setAttribute('stroke-width', '1');
    svgElement.appendChild(line);
  }
};

export const drawCents = (svgElement: SVGSVGElement) => {
  const width = svgElement.width.baseVal.value;
  const height = svgElement.height.baseVal.value;
  const step = width / 100;
  for (let x = step, cents = -49; x < width; x += step, cents += 1) {
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    line.setAttribute('x1', `${x}`);
    line.setAttribute('y1', '0');
    line.setAttribute('x2', `${x}`);
    line.setAttribute('y2', `${height - 1}`);
    if (cents === 0) {
      line.setAttribute('stroke', 'green');
    } else if (cents % 10 === 0) {
      line.setAttribute('stroke', '#333');
    } else {
      line.setAttribute('stroke', '#222');
    }
    line.setAttribute('stroke-width', '1');
    svgElement.appendChild(line);
  }
};

export const drawSpectrum = (
  svgElement: SVGSVGElement,
  spectrum: number[],
  minHz: number,
  hzPerBin: number,
  color: string = '#00ffff',
  offsetLeft: number = 0,
  offsetRight: number = 0
) => {
  let peakIndex = 0;
  for (let i = 0, n = spectrum.length; i < n; i += 1) {
    peakIndex = spectrum[i] > spectrum[peakIndex] ? i : peakIndex;
  }
  const x = d3
    .scaleLog()
    .domain([minHz, minHz + hzPerBin * (spectrum.length - 1)])
    .base(2)
    .range([offsetLeft, svgElement.width.baseVal.value - offsetRight]);
  const y = d3
    .scaleLinear()
    .domain([0, spectrum[peakIndex]])
    .range([svgElement.height.baseVal.value, 20]);

  const lineGenerator = d3
    .line<number>()
    .x((d, i) => x(minHz + hzPerBin * i))
    .y((d: number) => y(d));

  d3
    .select(svgElement)
    .append('g')
    .append('path')
    .datum(spectrum)
    .attr('fill', 'none')
    .attr('stroke', color)
    .attr('d', lineGenerator);

  return peakIndex;
};

export const drawSpectralMaxima = (
  svgElement: SVGSVGElement,
  spectrum: number[],
  peakIndex: number,
  maxima: number[],
  minHz: number,
  hzPerBin: number,
  color: string = '#00ffff',
  offsetLeft: number = 0,
  offsetRight: number = 0
) => {
  const points: [number, number][] = [];
  for (let i = 0; i < maxima.length; i += 1) {
    points[i] = [minHz + hzPerBin * maxima[i], spectrum[maxima[i]]];
  }
  const x = d3
    .scaleLog()
    .domain([minHz, minHz + hzPerBin * (spectrum.length - 1)])
    .base(2)
    .range([offsetLeft, svgElement.width.baseVal.value - offsetRight]);
  const y = d3
    .scaleLinear()
    .domain([0, spectrum[peakIndex]])
    .range([svgElement.height.baseVal.value, 20]);

  d3
    .select(svgElement)
    .append('g')
    .selectAll('dots')
    .data(points)
    .enter()
    .append('circle')
    .attr('class', 'dot')
    .attr('r', 3)
    .attr('cx', (d: number[], i: number) => x(d[0]))
    .attr('cy', (d: number[], i: number) => y(d[1]))
    .style('fill', color);
};

export const drawStrobe = (
  svgElement: SVGSVGElement,
  signal: Float32Array | number[],
  xDomain: [number, number]
) => {
  const height = svgElement.height.baseVal.value;
  const x = d3
    .scaleLinear()
    .domain(xDomain)
    .range([0, svgElement.width.baseVal.value]);

  let i = 0;
  while (i < signal.length) {
    const start = i;
    const sign = sighn(signal[i]);
    while (i < signal.length && sighn(signal[i]) === sign) {
      i += 1;
    }
    if (i > start) {
      const rect = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'rect'
      );
      rect.setAttribute('x', `${x(start)}`);
      rect.setAttribute('y', '0');
      rect.setAttribute('width', `${Math.abs(x(i) - x(start))}`);
      rect.setAttribute('height', `${height}`);
      rect.setAttribute('fill', sign > 0 ? 'white' : 'black');
      svgElement.appendChild(rect);
    }
  }
};
