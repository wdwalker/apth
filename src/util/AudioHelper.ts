// We need to do this or TypeScript complains.
declare namespace window {
  const AudioContext: {
    new (): AudioContext;
  };
  const webkitAudioContext: {
    new (): AudioContext;
  };
}

const AudioContext = window.AudioContext || window.webkitAudioContext || false;

export type Callback = (samples: Float32Array) => void;

/**
 * Helper interface to WebAudio.
 */
class AudioHelper {
  public readonly sampleRate: number;
  private bufferSize: number;
  private audioContext: any; // tslint:disable-line
  private callback: Callback;
  private stream: MediaStream;
  private sourceNode: MediaStreamAudioSourceNode;
  private bandpassNode: BiquadFilterNode;
  private gainNode: GainNode;
  private listenerNode: ScriptProcessorNode;

  /**
   * Create a new AudioHelper
   * @param  {number} sampleRate the sample rate we want
   * @param  {number} bufferSize how big of a recording buffer we want
   * @return {AudioHelper} the new AudioHelper
   */
  constructor(sampleRate: number, bufferSize: number) {
    if (!AudioContext) {
      alert(
        'This app will not work because WebAudio does not work in this browser.'
      );
      return;
    }
    this.audioContext = new AudioContext();
    this.sampleRate = sampleRate;
    this.bufferSize = bufferSize;
    this.onAudio = this.onAudio.bind(this);
  }

  /**
   * [startRecording description]
   * @param  {func}   onAudio function to call when we get audio input
   * @return {undefined}
   */
  startRecording(hz: number, callback: (samples: Float32Array) => void) {
    this.callback = callback;
    navigator.mediaDevices
      .getUserMedia({
        audio: true,
        video: false
      })
      .then((stream) => {
        this.stream = stream;
        // console.log('stream', stream);

        this.sourceNode = this.audioContext.createMediaStreamSource(
          this.stream
        );
        // console.log('sourceNode', this.sourceNode);

        this.bandpassNode = this.audioContext.createBiquadFilter();
        this.bandpassNode.type = 'bandpass';
        this.setF0(hz);
        this.setQ(500);
        // console.log('bandpassNode', this.bandpassNode);

        this.gainNode = this.audioContext.createGain();
        // console.log('gainNode', this.gainNode);

        this.listenerNode = this.audioContext.createScriptProcessor(
          this.bufferSize,
          1,
          1
        );
        this.listenerNode.onaudioprocess = this.onAudio;
        // console.log('listenerNode', this.listenerNode);

        this.sourceNode.connect(this.bandpassNode);
        this.bandpassNode.connect(this.gainNode);
        this.gainNode.connect(this.listenerNode);
        this.listenerNode.connect(this.audioContext.destination);
      })
      .catch((e) => {
        console.error(e);
        alert(e);
      });
  }

  /**
   * Stops recording.
   * @return {undefined}
   */
  stopRecording() {
    if (this.listenerNode) {
      this.listenerNode.disconnect();
    }
    if (this.gainNode) {
      this.gainNode.disconnect();
    }
    if (this.bandpassNode) {
      this.bandpassNode.disconnect();
    }
    this.sourceNode.disconnect();
    this.stream.getTracks()[0].stop();
  }

  setGain(gain: number) {
    if (this.gainNode) {
      this.gainNode.gain.linearRampToValueAtTime(
        gain,
        this.audioContext.currentTime
      );
    }
  }

  setQ(q: number) {
    if (this.bandpassNode) {
      this.bandpassNode.Q.linearRampToValueAtTime(
        q,
        this.audioContext.currentTime
      );
    }
  }

  setF0(hz: number) {
    if (this.bandpassNode) {
      this.bandpassNode.frequency.linearRampToValueAtTime(
        hz,
        this.audioContext.currentTime
      );
    }
  }

  // tslint:disable-next-line
  private onAudio(ape: any) {
    const inputBuffer = ape.inputBuffer.getChannelData(0);
    const outputBuffer = ape.outputBuffer.getChannelData(0);
    for (let i = 0; i < inputBuffer.length; i += 1) {
      outputBuffer[i] = inputBuffer[i];
    }
    this.callback(outputBuffer);
  }
}

export default AudioHelper;
