import * as React from 'react';

import 'react-select/dist/react-select.css';
import Select, { Option } from 'react-select';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import EqualTemperament from '../util/EqualTemperament';
import { KeyName, KEY_NAMES } from '../util/Piano';
import AudioHelper, { Callback } from '../util/AudioHelper';
import * as draw from '../util/draw';

import './BeatFilter.css';

interface Props {}
interface State {
  selectedKey: KeyName;
  audioHelper: AudioHelper;
  gain: number;
  listening: boolean;
  drawing: boolean;
}

const OPTIONS = KEY_NAMES.map((keyName: KeyName, i: number) => {
  return { label: keyName, value: i };
});

/**
 * Our "Beat Filter" page that displays and plays the bandpass analysis.
 */
class BeatFilter extends React.Component<Props, State> {
  /**
   * Our svg drawing area for D3
   */
  private svgElement: SVGSVGElement;
  private readonly DOMAIN_MAX: number = 10.0; // vertical range

  /**
   * How many seconds of filtered data to draw
   */
  private readonly SECONDS: number = 1;
  private filtered: number[]; // SECONDS of accumulated filtered signal

  constructor(props: Props) {
    super(props);
    this.state = {
      selectedKey: 'A5',
      audioHelper: new AudioHelper(44100, 16384),
      gain: 1.5,
      listening: false,
      drawing: true
    };
    this.onAudio = this.onAudio.bind(this);
    this.drawAudio = this.drawAudio.bind(this);
    this.filtered = [];
  }

  /**
   * Draws the filtered signal
   * @param signal the filtered samples
   * @param color  what color to use
   * @param offset index we should use as the beginning of the signal
   */
  drawSignal(signal: number[], color: string = '#00ffff', offset: number = 0) {
    draw.drawSignal(
      this.svgElement,
      signal,
      [0, this.SECONDS * this.state.audioHelper.sampleRate],
      [-this.DOMAIN_MAX, this.DOMAIN_MAX],
      color,
      offset
    );
  }

  /**
   * Append the filtered data to our accumulated arrays, calculating
   * the rolling RMS as soon as we have enough samples
   * @param filtered
   */
  accumulateAndTrim(filtered: Float32Array) {
    for (let i = 0; i < filtered.length; i += 1) {
      this.filtered[this.filtered.length] = filtered[i];
    }

    // Trim our accumulated arrays down to SECONDS worth of samples
    if (
      this.filtered.length >=
      this.SECONDS * this.state.audioHelper.sampleRate
    ) {
      this.filtered = this.filtered.slice(
        -(this.SECONDS * this.state.audioHelper.sampleRate)
      );
    }
  }

  drawAudio(samples: Float32Array) {
    this.accumulateAndTrim(samples);
    const svgElement = document.getElementById('filtered-signal');
    while (svgElement && svgElement.firstChild) {
      svgElement.removeChild(svgElement.firstChild);
    }
    this.drawSignal(this.filtered);
  }

  onAudio: Callback = (samples: Float32Array) => {
    if (samples.length && this.state.drawing) {
      setTimeout(this.drawAudio, 20, samples);
    }
  };

  render() {
    const { audioHelper, gain, listening } = this.state;
    const listenTitle = listening ? 'Turn off listening' : 'Turn on listening';
    return (
      <main id="beat-filter">
        <div id="signal-wrapper">
          <svg
            id="filtered-signal"
            ref={(ref: SVGSVGElement) => (this.svgElement = ref)}
          />
        </div>
        <button
          title={listenTitle}
          id="listen-button"
          type="button"
          data-toggle="button"
          aria-pressed={listening}
          className="btn"
          onClick={(e) => {
            if (!listening) {
              audioHelper.startRecording(
                EqualTemperament.hz(this.state.selectedKey),
                this.onAudio
              );
              audioHelper.setGain(10 ** gain);
            } else {
              audioHelper.stopRecording();
            }
            this.setState({ listening: !listening });
          }}
        >
          <span className="fa fa-microphone audio-button" />
        </button>
        <Select
          searchable={false}
          autosize={false}
          options={OPTIONS}
          value={KEY_NAMES.indexOf(this.state.selectedKey)}
          onChange={(target: Option<number>) => {
            if (target && target.value) {
              audioHelper.setF0(EqualTemperament.hz(KEY_NAMES[target.value]));
              this.setState({ selectedKey: KEY_NAMES[target.value] });
            }
          }}
        />
        <Slider
          min={0}
          max={3}
          step={0.1}
          vertical={true}
          value={this.state.gain}
          onChange={(value: number) => {
            audioHelper.setGain(10 ** value);
            this.setState({ gain: value });
          }}
        />
      </main>
    );
  }
}

export default BeatFilter;
