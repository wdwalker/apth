import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import BeatFilter from './components/BeatFilter';

ReactDOM.render(<BeatFilter />, document.getElementById('root') as HTMLElement);

registerServiceWorker();
